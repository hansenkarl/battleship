﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    /// <summary>
    /// Initializes db  
    /// </summary>
    public class AppDbContext : DbContext
    {
        public static readonly AppDbContext appDbContext = new AppDbContext();
        public DbSet<RulesDAL> RulesDALs { get; set; }
        public DbSet<GameDAL> GameDALs { get; set; }

        public RulesDAL GetPrimaryRulesDAL()
        {
            var rulesDal = RulesDALs.Find((long) 1);
            if (rulesDal == null)
            {
                rulesDal = new RulesDAL
                {
                    RulesDALId = 1,
                    Width = 10,
                    Height = 10,

                    ShipsMayTouch = false,

                    CarrierSize = 5,
                    BattleshipSize = 4,
                    SubmarineSize = 3,
                    CruiserSize = 2,
                    PatrolSize = 1,

                    CarrierCount = 1,
                    BattleshipCount = 1,
                    SubmarineCount = 1,
                    CruiserCount = 1,
                    PatrolCount = 1
                };
                RulesDALs.Add(rulesDal);
                appDbContext.SaveChanges();
            }
            return rulesDal;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(
                "Server=(localdb)\\mssqllocaldb;" + // server to use
                "Database=BattleShipDb;" + // database to use or create
                "Trusted_Connection=True;" + // no credentials needed, this is a local sql instance
                "MultipleActiveResultSets=true" // allow multiple parallel queries
                );

        }
    }

    
}
