﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL
{
    /// <summary>
    /// Info and structure required in order to load game or save a replay.
    /// </summary>
    public class GameDAL
    {
        public GameDAL(bool gameOver, string player1Ships, string player2Ships, string turns, string saveName, long timestamp, int winner)
        {         
            GameOver = gameOver;
            Player1Ships = player1Ships ?? throw new ArgumentNullException(nameof(player1Ships));       
            Player2Ships = player2Ships ?? throw new ArgumentNullException(nameof(player2Ships));
            Turns = turns ?? throw new ArgumentNullException(nameof(turns));
            SaveName = saveName ?? throw new ArgumentNullException(nameof(saveName));
            Timestamp = timestamp;
            Winner = winner;
        }

        public int GameDALId { get; set; }

        public bool GameOver { get; set; }

        [MaxLength(100000)]
        [Required]
        public string Player1Ships { get; set; }

        [MaxLength(100000)]
        [Required]
        public string Player2Ships { get; set; }

        [MaxLength(42000)]
        [Required]
        public string Turns { get; set; }

        [MaxLength(32)]
        public string SaveName { get; set; }

        public long Timestamp { get; set; }

        public int Winner { get; set; }

        
        public long RulesDALId { get; set; }
        public RulesDAL RulesDAL { get; set; }
    }
}
