﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class InitDbAgain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RulesDALs",
                columns: table => new
                {
                    RulesDALId = table.Column<long>(nullable: false),
                    Width = table.Column<int>(nullable: false),
                    Height = table.Column<int>(nullable: false),
                    ShipsMayTouch = table.Column<bool>(nullable: false),
                    CarrierSize = table.Column<int>(nullable: false),
                    BattleshipSize = table.Column<int>(nullable: false),
                    SubmarineSize = table.Column<int>(nullable: false),
                    CruiserSize = table.Column<int>(nullable: false),
                    PatrolSize = table.Column<int>(nullable: false),
                    CarrierCount = table.Column<int>(nullable: false),
                    BattleshipCount = table.Column<int>(nullable: false),
                    SubmarineCount = table.Column<int>(nullable: false),
                    CruiserCount = table.Column<int>(nullable: false),
                    PatrolCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RulesDALs", x => x.RulesDALId);
                });

            migrationBuilder.CreateTable(
                name: "GameDALs",
                columns: table => new
                {
                    GameDALId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GameOver = table.Column<bool>(nullable: false),
                    Player1Ships = table.Column<string>(maxLength: 100000, nullable: false),
                    Player2Ships = table.Column<string>(maxLength: 100000, nullable: false),
                    Turns = table.Column<string>(maxLength: 42000, nullable: false),
                    SaveName = table.Column<string>(maxLength: 32, nullable: true),
                    Timestamp = table.Column<long>(nullable: false),
                    Winner = table.Column<int>(nullable: false),
                    RulesDALId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameDALs", x => x.GameDALId);
                    table.ForeignKey(
                        name: "FK_GameDALs_RulesDALs_RulesDALId",
                        column: x => x.RulesDALId,
                        principalTable: "RulesDALs",
                        principalColumn: "RulesDALId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GameDALs_RulesDALId",
                table: "GameDALs",
                column: "RulesDALId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GameDALs");

            migrationBuilder.DropTable(
                name: "RulesDALs");
        }
    }
}
