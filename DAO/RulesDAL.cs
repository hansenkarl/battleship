﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL
{
    /// <summary>
    /// Rules that go into database
    /// </summary>
    public class RulesDAL
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long RulesDALId { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public bool ShipsMayTouch { get; set; }

        public int CarrierSize { get; set; }
        public int BattleshipSize { get; set; }
        public int SubmarineSize { get; set; }
        public int CruiserSize { get; set; }
        public int PatrolSize { get; set; }

        public int CarrierCount { get; set; }
        public int BattleshipCount { get; set; }
        public int SubmarineCount { get; set; }
        public int CruiserCount { get; set; }
        public int PatrolCount { get; set; }
    }
}
