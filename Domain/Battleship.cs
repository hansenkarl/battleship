﻿namespace Domain
{
    public class Battleship : Ship
    {
        public Battleship()
        {
            Length = Rules.ShipSizes["battleship"];
        }
    }
}