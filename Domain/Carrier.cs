﻿namespace Domain
{
    public class Carrier : Ship
    {
        public Carrier()
        {
            Length = Rules.ShipSizes["carrier"];
        }
    }
}