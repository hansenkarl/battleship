﻿namespace Domain
{
    public class Cruiser : Ship
    {
        public Cruiser()
        {
            Length = Rules.ShipSizes["cruiser"];
        }
    }
}