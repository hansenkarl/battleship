﻿using DAL;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using static Utilities.Utilities;


namespace Domain
{
    public class Game
    {
        public readonly GameMap Player1ShipMap;
        public readonly GameMap Player1AttackMap;
        public readonly GameMap Player2ShipMap;
        public readonly GameMap Player2AttackMap;
        public int Player1HitCount = 0;
        public int Player2HitCount = 0;
        public int ShipTileCount = 0;
        private int roundCount = 1;
        public List<Turn> turns;
        readonly string initialPlayer1Ships;
        readonly string initialPlayer2Ships;
        // 0 - warning screen,
        // 1 - ask if wanna save, go back or ask for coords
        // 2 - saving the game,
        // 3 - back to menu
        // 4 - handle/reask coords
        // 5 - show attack result
        int CurrentTurnPhase = 0;
        bool ForceGameOver = false;

        /// <summary>
        /// Create game.
        /// </summary>
        /// <param name="player1ShipMap"></param>
        /// <param name="player2ShipMap"></param>
        public Game(GameMap player1ShipMap, GameMap player2ShipMap)
        {
            foreach (var shipName in Rules.ShipNames)
            {
                ShipTileCount += Rules.FleetSize[shipName] * Rules.ShipSizes[shipName];
            }
            // since players can skip ships, we need to make sure that game counts hits correctly for the purposes of calculating the winner
            Player1HitCount = player2ShipMap.SkipTileCount;
            Player2HitCount = player1ShipMap.SkipTileCount;

            Player1ShipMap = player1ShipMap;
            Player1AttackMap = new GameMap();
            Player2ShipMap = player2ShipMap;
            Player2AttackMap = new GameMap();
            initialPlayer1Ships = Player1ShipMap.ToString();
            initialPlayer2Ships = Player2ShipMap.ToString();
        }

        /// <summary>
        /// Run console game.
        /// </summary>
        public void Run()
        {
            if (turns == null) turns = new List<Turn>();           
            while (!IsGameOver())
            {
                int activePlayer = 2 - roundCount % 2;
                int passivePlayer = 1 + roundCount % 2;
                Console.Clear();
                var text = AskForUserInput(
                    "Player " + passivePlayer + " look away! Player " + activePlayer +
                    "'s turn! Press Enter to continue, b to return to main menu, or s to save the game!", false, true);
                if (text.Equals("b")) return;
                if (text.Equals("s"))
                {
                    Console.WriteLine("If you want to save this game's state, type its name, otherwise, press enter to continue!");
                    string save = Console.ReadLine().Trim().ToLower();
                    if (save != "")
                    {
                        (RulesDAL gameRules, GameDAL gameProgress) = GameDalMaker(
                            false, initialPlayer1Ships, initialPlayer2Ships, turns, save, GetCurrentTime(), 1 + roundCount % 2);
                        var db = AppDbContext.appDbContext;
                        db.RulesDALs.Add(gameRules);
                        gameProgress.RulesDAL = gameRules;
                        db.GameDALs.Add(gameProgress);
                        db.SaveChanges();
                        Console.WriteLine("Game state saved! Press enter to continue!");
                        Console.ReadLine();
                        return;
                    }
                }

                Console.Clear();
                GameMap shipMap;
                GameMap enemyShipMap;
                GameMap attackMap;

                if (activePlayer == 1)
                {
                    shipMap = Player1ShipMap;
                    attackMap = Player1AttackMap;
                    enemyShipMap = Player2ShipMap;
                }
                else
                {
                    shipMap = Player2ShipMap;
                    attackMap = Player2AttackMap;
                    enemyShipMap = Player1ShipMap;
                }

                OutputMapConsole(shipMap.RenderMap(), attackMap.RenderMap());

                char col;
                int row;

                do
                {
                    string input = AskForUserInput("Where to fire?", true, false);
                    col = input[0];
                    int.TryParse(input.Substring(1), out row);
                } while (!CanAttack(attackMap, col, row));

                Attack(enemyShipMap, attackMap, col, row, activePlayer);

                turns.Add(new Turn(activePlayer, col, row));

                Console.Clear();
                OutputMapConsole(shipMap.RenderMap(), attackMap.RenderMap());
                AskForUserInput("Press enter to continue!", false, true);

                roundCount++;
            }           
            Winner();
            Console.WriteLine("To save this game's replay, type its name, otherwise, press enter to continue!");
            string question = Console.ReadLine().Trim().ToLower();
            if (question != "")
            {
                (RulesDAL gameRules, GameDAL gameProgress) = GameDalMaker(
                    true, initialPlayer1Ships, initialPlayer2Ships, turns, question, GetCurrentTime(), 1 + roundCount % 2);
                var db = AppDbContext.appDbContext;
                db.RulesDALs.Add(gameRules);
                gameProgress.RulesDAL = gameRules;
                db.GameDALs.Add(gameProgress);
                db.SaveChanges();
                Console.WriteLine("Replay saved! Press enter to continue!");
                Console.ReadLine();
            }           
            
        }

        /// <summary>
        /// Run one turn in web.
        /// </summary>
        /// <param name="userInput"></param>
        /// <returns></returns>
        public string RunOneTurn(string userInput)
        {
            if (turns == null) turns = new List<Turn>();
            int activePlayer = 2 - roundCount % 2;
            int passivePlayer = 1 + roundCount % 2;

            GameMap shipMap;
            GameMap enemyShipMap;
            GameMap attackMap;

            if (activePlayer == 1)
            {
                shipMap = Player1ShipMap;
                attackMap = Player1AttackMap;
                enemyShipMap = Player2ShipMap;
            }
            else
            {
                shipMap = Player2ShipMap;
                attackMap = Player2AttackMap;
                enemyShipMap = Player1ShipMap;
            }

            switch (CurrentTurnPhase)
            {
                case 0:
                    {
                        CurrentTurnPhase++;
                        return "Player " + passivePlayer + " look away! Player " + activePlayer +
                "'s turn! Press Enter to continue, b to return to main menu, or s to save the game!";
                    }
                case 1:
                    {
                        switch (userInput)
                        {
                            case "b":
                                ForceGameOver = true;
                                return null;
                            case "s":
                                CurrentTurnPhase++;
                                return "If you want to save this game's state, type its name, otherwise, press enter to continue!";
                            default:                    
                                CurrentTurnPhase += 3;
                                return OutputMapWeb(shipMap.RenderMap(), attackMap.RenderMap()) + "\n" + "Where to fire?";
                        }
                        
                    }
                case 2:
                    if (userInput == "")
                    {
                        CurrentTurnPhase += 2;
                        RunOneTurn(userInput);
                    }
                    else
                    {
                        CurrentTurnPhase++;
                        (RulesDAL gameRules, GameDAL gameProgress) = GameDalMaker(
                            false, initialPlayer1Ships, initialPlayer2Ships, turns, userInput, GetCurrentTime(), 1 + roundCount % 2);
                        var db = AppDbContext.appDbContext;
                        db.RulesDALs.Add(gameRules);
                        gameProgress.RulesDAL = gameRules;
                        db.GameDALs.Add(gameProgress);
                        db.SaveChanges();
                        return "Game state saved! Press enter to continue!";
                    }                        
                    break;
                case 3:
                    {
                        ForceGameOver = true;
                        return null;
                    }
                case 4:
                    {
                        if (userInput != null && (userInput.Length == 2 || userInput.Length == 3))
                        {
                            char col = userInput[0];
                            bool passed = int.TryParse(userInput.Substring(1), out int row);

                            if (passed && CanAttack(attackMap, col, row))
                            {
                                Attack(enemyShipMap, attackMap, col, row, activePlayer);
                                turns.Add(new Turn(activePlayer, col, row));
                                roundCount++;
                                CurrentTurnPhase = 0;
                                return OutputMapWeb(shipMap.RenderMap(), attackMap.RenderMap()) + "\nPress enter to continue!";
                            }
                        }
                        
                        string errorMsg = "";
                        if (!(userInput == "" || userInput == null)) errorMsg = "\nInvalid coordinate!";
                        return OutputMapWeb(shipMap.RenderMap(), attackMap.RenderMap()) + errorMsg + "\nWhere to fire?";  
                    }
            }
            return null;
        }

        /// <summary>
        /// Simulates game until the state we need to load is reached.
        /// </summary>
        public void Simulate()
        {
            foreach (Turn turn in turns)
            {
                int activePlayer = 2 - roundCount % 2;
                int passivePlayer = 1 + roundCount % 2;
                GameMap shipMap;
                GameMap enemyShipMap;
                GameMap attackMap;

                if (activePlayer == 1)
                {
                    shipMap = Player1ShipMap;
                    attackMap = Player1AttackMap;
                    enemyShipMap = Player2ShipMap;
                }
                else
                {
                    shipMap = Player2ShipMap;
                    attackMap = Player2AttackMap;
                    enemyShipMap = Player1ShipMap;
                }

                Attack(enemyShipMap, attackMap, turn.col, turn.row, activePlayer);
                roundCount++;
            }
        }

        /// <summary>
        /// Attack!
        /// </summary>
        /// <param name="shipMap">attacked player's map</param>
        /// <param name="attackMap">attacking player's map</param>
        /// <param name="col">column</param>
        /// <param name="row">row</param>
        /// <param name="playerNr">attacking player</param>
        /// <returns>hit or miss</returns>
        private bool Attack(GameMap shipMap, GameMap attackMap, char col, int row, int playerNr)
        {
            if (shipMap.GetTile(col, row) == TileTypes.Ship)
            {
                attackMap.SetTile(col, row, TileTypes.Hit);
                shipMap.SetTile(col, row, TileTypes.Hit);
                if (playerNr == 1) Player1HitCount++;
                else Player2HitCount++;
                return true;
            }
            else
            {
                attackMap.SetTile(col, row, TileTypes.Miss);
                shipMap.SetTile(col, row, TileTypes.Miss);
                return false;
            }
        }

        /// <summary>
        /// Check if attacking on this coordinate is possible.
        /// </summary>
        /// <param name="attackMap"></param>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <returns>is this attack possible</returns>
        private bool CanAttack(GameMap attackMap, char col, int row)
        {
            return (Alphabet.IndexOf(col) >= 0 && Alphabet.IndexOf(col) < Rules.width) &&
                   (row >= 1 && row <= Rules.height) &&
                   (attackMap.GetTile(col, row) == TileTypes.Empty);
        }


        public bool IsGameOver()
        {
            return ShipTileCount == Player1HitCount || ShipTileCount == Player2HitCount || ForceGameOver;
        }

        public void Winner()
        {
            if (ShipTileCount == Player1HitCount) Console.WriteLine("Congratulations player 1, you won!");
            else if (ShipTileCount == Player2HitCount) Console.WriteLine("Congratulations player 2, you won!");
        }

        /// <summary>
        /// Console mode only.
        /// </summary>
        /// <param name="question">message to the user</param>
        /// <param name="coords">ask for coordinates</param>
        /// <param name="blank">is this an info screen</param>
        /// <returns>coordinate string, integer string or a random thing</returns>
        private static string AskForUserInput(string question, bool coords, bool blank)
        {
            Console.WriteLine(question);

            if (blank)
            {
                return Console.ReadLine();
            }
            else
            {
                while (true)
                {
                    string input = Console.ReadLine().Trim().ToLower();
                    if (coords)
                    {
                        if (input.Length > 1 && Alphabet.Contains(input[0]) &&
                            int.TryParse(input.Substring(1), out var n)) return input;
                        else Console.WriteLine("Not a coordinate! Try again!");
                    }
                    else
                    {
                        if (int.TryParse(input, out var value)) return value.ToString();
                        else Console.WriteLine("Not a number! Try again!");
                    }
                }
            }
        }

        private static void OutputMapConsole(List<string> shipMap, List<string> attackMap)
        {
            Console.Clear();
            for (int i = 0; i < shipMap.Count; i++)
            {
                Console.Write(shipMap[i]);
                Console.Write("       ");
                Console.Write(attackMap[i]);
                Console.WriteLine();
            }
        }

        private static string OutputMapWeb(List<string> shipMap, List<string> attackMap)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < shipMap.Count; i++)
            {
                sb.Append(shipMap[i]);
                sb.Append("       ");
                sb.Append(attackMap[i]);
                sb.AppendLine();
            }
            return sb.ToString();
        }

        /// <summary>
        /// Shows the last shot in the web version.
        /// </summary>
        /// <returns></returns>
        public string OutputWebLastShotScreen()
        {
            int activePlayer = 2 - roundCount % 2;
            int passivePlayer = 1 + roundCount % 2;

            GameMap shipMap;
            GameMap enemyShipMap;
            GameMap attackMap;

            if (activePlayer == 1)
            {
                shipMap = Player1ShipMap;
                attackMap = Player1AttackMap;
                enemyShipMap = Player2ShipMap;
            }
            else
            {
                shipMap = Player2ShipMap;
                attackMap = Player2AttackMap;
                enemyShipMap = Player1ShipMap;  
            }
            return OutputMapWeb(shipMap.RenderMap(), attackMap.RenderMap()) + "\nPress enter to continue!";
        }


        /// <summary>
        /// Creating DAL objects for game details that the players might want to save.
        /// </summary>
        /// <param name="gameOver"></param>
        /// <param name="player1Ships"></param>
        /// <param name="player2Ships"></param>
        /// <param name="turns"></param>
        /// <param name="saveName"></param>
        /// <param name="timestamp"></param>
        /// <param name="winner"></param>
        /// <returns></returns>
        public static (RulesDAL, GameDAL) GameDalMaker(bool gameOver ,string player1Ships,
            string player2Ships, List<Turn> turns, string saveName, long timestamp, int winner)
        {
            RulesDAL rd = new RulesDAL
            {
                RulesDALId = GetCurrentTime(),
                Width = Rules.width,
                Height = Rules.height,
                ShipsMayTouch = Rules.shipsMayTouch,
                CarrierSize = Rules.ShipSizes["carrier"],
                BattleshipSize = Rules.ShipSizes["battleship"],
                SubmarineSize = Rules.ShipSizes["submarine"],
                CruiserSize = Rules.ShipSizes["cruiser"],
                PatrolSize = Rules.ShipSizes["patrol"],
                CarrierCount = Rules.FleetSize["carrier"],
                BattleshipCount = Rules.FleetSize["battleship"],
                SubmarineCount = Rules.FleetSize["submarine"],
                CruiserCount = Rules.FleetSize["cruiser"],
                PatrolCount = Rules.FleetSize["patrol"]
            };
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < turns.Count; i++)
            {
                sb.Append(turns[i]);
                if (i < turns.Count - 1) sb.Append(",");
            }            
            return (rd, new GameDAL(gameOver, player1Ships, player2Ships, sb.ToString(), saveName, timestamp, winner));
        }

        /// <summary>
        /// To save replays in web.
        /// </summary>
        /// <param name="name"></param>
        public void SaveReplay(string name)
        {
            (RulesDAL gameRules, GameDAL gameProgress) = GameDalMaker(
               true, initialPlayer1Ships, initialPlayer2Ships, turns, name, GetCurrentTime(), 1 + roundCount % 2);
            var db = AppDbContext.appDbContext;
            db.RulesDALs.Add(gameRules);
            gameProgress.RulesDAL = gameRules;
            db.GameDALs.Add(gameProgress);
            db.SaveChanges();
        }
    }
}