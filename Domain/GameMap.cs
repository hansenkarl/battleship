﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using static Utilities.Utilities;

namespace Domain
{
    public class GameMap
    {
        private readonly Dictionary<string, string> _tiles;

        public int SkipTileCount = 0;

        public GameMap()
        {
            _tiles = new Dictionary<string, string>();
        }

        /// <summary>
        /// deserialize from XML
        /// </summary>
        /// <param name="data"></param>
        public GameMap(string data)
        {
            XElement xElem2 = XElement.Parse(data);
            _tiles = xElem2.Descendants("entry").ToDictionary(x => (string)x.Attribute("coord"), x => (string)x.Attribute("tile"));
        }

        /// <summary>
        /// returns one of the constants from TileTypes
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public string GetTile(char col, int row)
        {
            return !_tiles.ContainsKey(col + row.ToString()) ? TileTypes.Empty : _tiles[col + row.ToString()];
        }

        public void SetTile(char col, int row, string setTo)
        {
            _tiles[col + row.ToString()] = setTo;
        }


        public bool SetShip(char col, int row, Ship ship)
        {
            if (ship.Direction != Ship.Directions.Undecided && CanPlace(col, row, ship))
            {
                int colIndex = Alphabet.IndexOf(col);
                (int dirCol, int dirRow) = DirectionDeltas(ship.Direction);
                for (int i = 0; i < ship.Length; i++)
                {
                    char curCol = Alphabet[i * dirCol + colIndex];
                    int curRow = i * dirRow + row;
                    SetTile(curCol, curRow, TileTypes.Ship);
                }

                return true;
            }

            return false;
        }

        private bool CanPlace(char col, int row, Ship ship)
        {
            if (ship.Direction == Ship.Directions.Undecided)
            {
                return false;
            }

            int colIndex = Alphabet.IndexOf(col);
            (int dirCol, int dirRow) = DirectionDeltas(ship.Direction);

            for (int i = 0; i < ship.Length; i++)
            {
                int curColIndex = i * dirCol + colIndex;
                if (curColIndex >= Rules.width || curColIndex < 0) return false;
                char curCol = Alphabet[curColIndex];
                int curRow = i * dirRow + row;
                if (curRow > Rules.height || curRow < 1) return false;
                if (GetTile(curCol, curRow) != TileTypes.Empty ||
                    (!Rules.shipsMayTouch && CheckForTouching(curCol, curRow)))
                {
                    return false;
                }
            }

            return true;
        }

        private bool CheckForTouching(char col, int row)
        {
            // col min 0, max Game.width - 1
            // row min 1, max Game.height
            int ci = Alphabet.IndexOf(col);
            return
                (row - 1 >= 0 && GetTile(Alphabet[ci], row - 1) != TileTypes.Empty) || // top
                (ci < Rules.width && row - 1 >= 0 &&
                 GetTile(Alphabet[ci + 1], row - 1) != TileTypes.Empty) || // top right
                (ci + 1 < Rules.width && GetTile(Alphabet[ci + 1], row) != TileTypes.Empty) || // right
                (ci + 1 < Rules.width && row <= Rules.height &&
                 GetTile(Alphabet[ci + 1], row + 1) != TileTypes.Empty) || // bottom right
                (row + 1 <= Rules.height && GetTile(Alphabet[ci], row + 1) != TileTypes.Empty) || // bottom
                (ci - 1 >= 0 && row <= Rules.height &&
                 GetTile(Alphabet[ci - 1], row + 1) != TileTypes.Empty) || // bottom left
                (ci - 1 >= 0 && GetTile(Alphabet[ci - 1], row) != TileTypes.Empty) || // left
                (ci - 1 >= 0 && row >= 1 && GetTile(Alphabet[ci - 1], row - 1) != TileTypes.Empty); // top left
        }

        public List<string> RenderMap()
        {
            var result = new List<string>();

            var line = "   ";
            for (var i = 0; i < Rules.width; i++)
            {
                line += "  " + Alphabet[i] + " ";
            }

            line += " ";
            result.Add(line);

            for (int i = 1; i <= Rules.height; i++)
            {
                result.Add("   " + DrawHorizontalBounds());
                line = "";
                if (i < 10) line += " ";
                line += i + " " + DrawVerticalBounds(i);
                result.Add(line);
            }

            result.Add("   " + DrawHorizontalBounds());

            return result;
        }

        private string DrawHorizontalBounds()
        {
            var sb = new StringBuilder();
            for (int i = 0; i < Rules.width; i++)
            {
                sb.Append("+ - ");
            }

            sb.Append("+");
            return sb.ToString();
        }

        private string DrawVerticalBounds(int row)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < Rules.width; i++)
            {
                sb.Append("| " + GetTile(Alphabet[i], row) + " ");
            }

            sb.Append("|");
            return sb.ToString();
        }

        /*
         * Parse direction for placing the ship.
         * returns a tuple containing two direction coefficients.
         */
        private static Tuple<int, int> DirectionDeltas(Ship.Directions direction)
        {
            int dirCol = 0;
            int dirRow = 0;
            switch (direction)
            {
                case Ship.Directions.West:
                    {
                        dirCol = 1;
                        break;
                    }
                case Ship.Directions.East:
                    {
                        dirCol = -1;
                        break;
                    }
                case Ship.Directions.North:
                    {
                        dirRow = 1;
                        break;
                    }
                case Ship.Directions.South:
                    {
                        dirRow = -1;
                        break;
                    }
            }

            return new Tuple<int, int>(dirCol, dirRow);
        }

        /// <summary>
        /// Serialize to XML.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            XElement xElem = new XElement(
                    "tiles",
                    _tiles.Select(x => new XElement("entry", new XAttribute("coord", x.Key), new XAttribute("tile", x.Value)))
                 );
            return xElem.ToString();
        }
    }
}