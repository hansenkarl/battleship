﻿using DAL;
using System;
using System.Collections.Generic;
using static Utilities.Utilities;

namespace Domain
{
    public static class Launcher
    {
        public static void LaunchRandom()
        {
            GameMap player1ShipMap = new GameMap();
            GameMap player2ShipMap = new GameMap();
            GenerateRandomStart(player1ShipMap);
            GenerateRandomStart(player2ShipMap);
            Game game = new Game(player1ShipMap, player2ShipMap);
            game.Run();
        }

        public static void LaunchCustom()
        {
            GameMap player1ShipMap = new GameMap();
            GameMap player2ShipMap = new GameMap();
            AskForCustomStart(player1ShipMap, "Player 1", "Player 2");
            AskForCustomStart(player2ShipMap, "Player 2", "Player 1");
            Game game = new Game(player1ShipMap, player2ShipMap);
            game.Run();
        }

        public static void LaunchSaved(GameDAL gameDAL)
        {
            var rules = gameDAL.RulesDAL;
            Rules.Load(rules);
            GameMap player1ShipMap = new GameMap(gameDAL.Player1Ships);
            GameMap player2ShipMap = new GameMap(gameDAL.Player2Ships);
            Game game = new Game(player1ShipMap, player2ShipMap)
            {
                turns = DeserializeTurns(gameDAL.Turns)
            };
            game.Simulate();
            game.Run();
            Rules.Init();
        }

        public static void LaunchReplay(GameDAL gameDAL)
        {
            var rules = gameDAL.RulesDAL;
            Rules.Load(rules);
            GameMap player1ShipMap = new GameMap(gameDAL.Player1Ships);
            GameMap player2ShipMap = new GameMap(gameDAL.Player2Ships);
            Replayer replayer = new Replayer(player1ShipMap, player2ShipMap, DeserializeTurns(gameDAL.Turns), gameDAL.Winner);
            replayer.Run();
            Rules.Init();
        }

        public static List<Turn> DeserializeTurns(string data)
        {
            List<Turn> turns = new List<Turn>();
            foreach (string turn in data.Split(','))
            {
                turns.Add(new Turn(turn));
            }
            return turns;
        }

        public static void GenerateRandomStart(GameMap shipMap)
        {
            foreach (KeyValuePair<string, int> entry in Rules.FleetSize)
            {
                for (int i = 0; i < entry.Value; i++)
                {
                    Ship ship = null;
                    switch (entry.Key)
                    {
                        case "carrier":
                            ship = new Carrier();
                            break;
                        case "battleship":
                            ship = new Battleship();
                            break;
                        case "submarine":
                            ship = new Submarine();
                            break;
                        case "cruiser":
                            ship = new Cruiser();
                            break;
                        case "patrol":
                            ship = new Patrol();
                            break;
                    }

                    for (int retries = 0; retries < 10000; retries++)
                    {
                        int dir = Random(1, 4);
                        int col = Random(0, Rules.width - 1);
                        int row = Random(1, Rules.height);

                        switch (dir)
                        {
                            case 1:
                                ship.Direction = Ship.Directions.North;
                                break;

                            case 2:
                                ship.Direction = Ship.Directions.East;
                                break;

                            case 3:
                                ship.Direction = Ship.Directions.West;
                                break;

                            case 4:
                                ship.Direction = Ship.Directions.South;
                                break;
                        }

                        if (shipMap.SetShip(Alphabet[col], row, ship)) break;
                    }
                }
            }
        }

        /// <summary>
        /// Player name such as 
        /// </summary>
        /// <param name="shipMap"></param>
        /// <param name="active">for example: "Player 1"</param>
        /// <param name="passive">for example: "Player 2"</param>
        public static void AskForCustomStart(GameMap shipMap, string active, string passive)
        {
            Console.Clear();
            Console.WriteLine(active + " where do you want your ships? " + passive + " look away!");
            Console.ReadLine();
            foreach (KeyValuePair<string, int> entry in Rules.FleetSize)
            {
                for (int i = 0; i < entry.Value; i++)
                {
                    Ship ship = null;
                    switch (entry.Key)
                    {
                        case "carrier":
                            ship = new Carrier();
                            break;
                        case "battleship":
                            ship = new Battleship();
                            break;
                        case "submarine":
                            ship = new Submarine();
                            break;
                        case "cruiser":
                            ship = new Cruiser();
                            break;
                        case "patrol":
                            ship = new Patrol();
                            break;
                    }

                    while (true)
                    {
                        Console.Clear();
                        foreach (var line in shipMap.RenderMap())
                        {
                            Console.WriteLine(line);
                        }

                        Console.WriteLine("Place your " + entry.Key + " (size " + Rules.ShipSizes[entry.Key] + ")");
                        Console.WriteLine(
                            "Enter a starting coordinate (e.g. a1) and for 90 degrees tilt insert + (e.g. a1+) or enter s to skip");

                        string input = Console.ReadLine().Trim().ToLower();
                        if (input.Equals("s"))
                        {
                            shipMap.SkipTileCount += Rules.ShipSizes[entry.Key];
                            break;
                        }
                        if (input.EndsWith("+"))
                        {
                            ship.Direction = Ship.Directions.West;
                            input = input.Substring(0, input.Length - 1);
                        }
                        else ship.Direction = Ship.Directions.North;

                        if (input.Length != 2 && input.Length != 3)
                        {
                            Console.WriteLine("Invalid coordinate! Press enter to retry!");
                            Console.ReadLine();
                            continue;
                        }

                        char col = input[0];
                        int.TryParse(input.Substring(1), out var row);

                        if (shipMap.SetShip(col, row, ship)) break;
                        else Console.WriteLine("Cannot place! Press enter to retry!");
                        Console.ReadLine();
                    }
                }
            }
            Console.Clear();
            foreach (var line in shipMap.RenderMap())
            {
                Console.WriteLine(line);
            }

            Console.WriteLine("All ships placed! Press enter to continue!");
            Console.ReadLine();
        }
    }
}