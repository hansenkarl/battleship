﻿namespace Domain
{
    public class Patrol : Ship
    {
        public Patrol()
        {
            Length = Rules.ShipSizes["patrol"];
        }
    }
}