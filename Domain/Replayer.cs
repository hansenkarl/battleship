﻿using DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Replayer
    {
        private readonly GameMap player1Ships;
        private readonly GameMap player2Ships;
        public readonly List<Turn> turns;
        public readonly int winner;
        public int turnCount = 1;

        public Replayer(GameMap player1Ships, GameMap player2Ships, List<Turn> turns, int winner)
        {
            this.player1Ships = player1Ships;
            this.player2Ships = player2Ships;
            this.turns = turns;
            this.winner = winner;
        }

        public void Run()
        {           
            foreach (Turn turn in turns)
            {
                Console.Clear();
                Render();
                GameMap defender = (turn.player == 2) ? player1Ships : player2Ships;
                Attack(defender, turn.col, turn.row);
                turnCount++;
                Console.WriteLine("Press enter to view next turn or q to quit viewing this replay!");
                string input = Console.ReadLine().Trim().ToLower();
                if ("q".Equals(input)) return;
            }
            Console.Clear();
            Render();
            Console.WriteLine("Player " + winner + " won! Press enter to continue!");
            Console.ReadLine();
        }

        /// <summary>
        /// For running replay in web.
        /// </summary>
        public string RunOneTurn()
        {
            Turn turn = turns[turnCount - 1];            
            GameMap defender = (turn.player == 2) ? player1Ships : player2Ships;
            Attack(defender, turn.col, turn.row);
            turnCount++;
            return OutputMap();
        }

        public string OutputMap()
        {
            List<string> leftMap = player1Ships.RenderMap();
            List<string> rightMap = player2Ships.RenderMap();
            StringBuilder sb = new StringBuilder();
            sb.Append("Turn " + turnCount + "\n");
            sb.Append("Player 1's fleet".PadRight(leftMap[0].Length, ' ') + "        Player 2's fleet\n");
            for (int i = 0; i < leftMap.Count; i++)
            {
                sb.Append(leftMap[i]);
                sb.Append("        ");
                sb.Append(rightMap[i]);
                sb.AppendLine();
            }
            return sb.ToString();
        }

        private void Attack(GameMap shipMap, char col, int row)
        {
            if (shipMap.GetTile(col, row) == TileTypes.Ship)
            {
                shipMap.SetTile(col, row, TileTypes.Hit);
            }
            else
            {
                shipMap.SetTile(col, row, TileTypes.Miss);
            }
        }

        private void Render()
        {
            List<string> leftMap = player1Ships.RenderMap();
            List<string> rightMap = player2Ships.RenderMap();
            Console.WriteLine("Turn " + turnCount);
            Console.WriteLine("Player 1's fleet".PadRight(leftMap[0].Length, ' ') + "        Player 2's fleet");
            for (int i = 0; i < leftMap.Count; i++)
            {
                Console.Write(leftMap[i]);
                Console.Write("       ");
                Console.Write(rightMap[i]);
                Console.WriteLine();
            }
        }
    }
}
