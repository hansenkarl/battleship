﻿using DAL;
using System.Collections.Generic;

namespace Domain
{
    public static class Rules
    {
        public static bool shipsMayTouch = false;
        public static int width = 10;
        public static int height = 10;

        public static List<string> ShipNames = new List<string>
        {
            "carrier",
            "battleship",
            "submarine",
            "cruiser",
            "patrol"
        };

        public static Dictionary<string, int> FleetSize = new Dictionary<string, int>
        {
            {"carrier", 1},
            {"battleship", 1},
            {"submarine", 1},
            {"cruiser", 1},
            {"patrol", 1}
        };


        public static Dictionary<string, int> ShipSizes = new Dictionary<string, int>
        {
            {"carrier", 5},
            {"battleship", 4},
            {"submarine", 3},
            {"cruiser", 2},
            {"patrol", 1}
        };

        public static void Init()
        {          
            Load(AppDbContext.appDbContext.GetPrimaryRulesDAL());           

        }

        public static void Load(RulesDAL savedRules)
        {
            width = savedRules.Width;
            height = savedRules.Height;

            shipsMayTouch = savedRules.ShipsMayTouch;

            FleetSize["carrier"] = savedRules.CarrierCount;
            FleetSize["battleship"] = savedRules.BattleshipCount;
            FleetSize["submarine"] = savedRules.SubmarineCount;
            FleetSize["cruiser"] = savedRules.CruiserCount;
            FleetSize["patrol"] = savedRules.PatrolCount;

            ShipSizes["carrier"] = savedRules.CarrierSize;
            ShipSizes["battleship"] = savedRules.BattleshipSize;
            ShipSizes["submarine"] = savedRules.SubmarineSize;
            ShipSizes["cruiser"] = savedRules.CruiserSize;
            ShipSizes["patrol"] = savedRules.PatrolSize;
        }

        public static void Save()
        {
            var rulesDal = AppDbContext.appDbContext.GetPrimaryRulesDAL();
            rulesDal.ShipsMayTouch = shipsMayTouch;
            rulesDal.Width = width;
            rulesDal.Height = height;

            rulesDal.CarrierCount = FleetSize["carrier"];
            rulesDal.BattleshipCount = FleetSize["battleship"];
            rulesDal.SubmarineCount = FleetSize["submarine"];
            rulesDal.CruiserCount = FleetSize["cruiser"];
            rulesDal.PatrolCount = FleetSize["patrol"];

            rulesDal.CarrierSize = ShipSizes["carrier"];
            rulesDal.BattleshipSize = ShipSizes["battleship"];
            rulesDal.SubmarineSize = ShipSizes["submarine"];
            rulesDal.CruiserSize = ShipSizes["cruiser"];
            rulesDal.PatrolSize = ShipSizes["patrol"];

            AppDbContext.appDbContext.SaveChanges();
        }
    }
}