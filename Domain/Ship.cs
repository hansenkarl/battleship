﻿namespace Domain
{
    /// <summary>
    /// Model class for different types of ships.
    /// </summary>
    public abstract class Ship
    {
        public string Name { get; set; }
        public int Length { get; set; }

        public enum Directions
        {
            North,
            East,
            South,
            West,
            Undecided
        }

        public Directions Direction { get; set; }
    }
}