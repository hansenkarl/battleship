﻿namespace Domain
{
    public class Submarine : Ship
    {
        public Submarine()
        {
            Length = Rules.ShipSizes["submarine"];
        }
    }
}