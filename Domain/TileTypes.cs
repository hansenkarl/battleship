﻿namespace Domain
{
    public class TileTypes
    {
        public const string Empty = " ";
        public const string Ship = "#";
        public const string Hit = "X";
        public const string Miss = "O";
    }
}