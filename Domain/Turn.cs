﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{  
    public class Turn
    {
        public readonly int player;
        public readonly char col;
        public readonly int row;
        
        /// <summary>
        /// Data object representing a player's turn.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="col"></param>
        /// <param name="row"></param>
        public Turn(int player, char col, int row)
        {
            this.player = player;
            this.col = col;
            this.row = row;         
        }

        /// <summary>
        /// Deserializes turn.
        /// </summary>
        /// <param name="data"></param>
        public Turn(string data)
        {
            string[] result = data.Split(' ');
            player = int.Parse(result[0]);
            col = char.Parse(result[1]);
            row = int.Parse(result[2]);
        }

        /// <summary>
        /// Serialize turn.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return player.ToString() + " " + col.ToString() + " " + row.ToString();
        }
    }
}
