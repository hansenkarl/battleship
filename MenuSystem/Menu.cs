﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain;

namespace MenuSystem
{
    public static class Menu
    {
        public static bool Running = true;
        internal static string CurrentMenu = "main";

        private static Dictionary<string, SortedDictionary<char, MenuItem>> Menus = new Dictionary<string, SortedDictionary<char, MenuItem>>
        {
            {
                "main", new SortedDictionary<char, MenuItem>
                {
                    {'1', new MenuItem('1', "New Game", MenuActions.NewGameMenu)},
                    {'2', new MenuItem('2', "Load Game", MenuActions.LoadGameMenu)},
                    {'3', new MenuItem('3', "Replays", MenuActions.ReplayMenu)},
                    {'4', new MenuItem('4', "Options", MenuActions.Options)},
                    {'5', new MenuItem('5', "Quit Game", MenuActions.QuitGame)}
                }
            },
            {
                "options", new SortedDictionary<char, MenuItem>
                {
                    {'1', new MenuItem('1', "Back to main", MenuActions.BackToMain)},
                    {'2', new MenuItem('2', "Rule: Boats may touch", MenuActions.ShipsMayTouch)},
                    {'3', new MenuItem('3', "Rule: Map size", MenuActions.EditMapSize)},
                    {'4', new MenuItem('4', "Rule: Fleet size", MenuActions.EditFleetSize)},
                    {'5', new MenuItem('5', "Rule: Ship sizes", MenuActions.EditShipSizes)}
                }
            },
            {
                "newGame", new SortedDictionary<char, MenuItem>
                {
                    {'1', new MenuItem('1', "Back to main", MenuActions.BackToMain)},
                    {'2', new MenuItem('2', "Random starting positions", MenuActions.NewGameRandom)},
                    {'3', new MenuItem('3', "Custom starting positions", MenuActions.NewGameCustom)}
                }
            },
        };


        // return screen state
        public static string GetMenu()
        {
            StringBuilder menu = new StringBuilder();
            menu.Append("Battleship\n\n");

            foreach (var menuItem in Menus[CurrentMenu].Values)
            {
                menu.Append(menuItem).Append("\n");
            }

            return menu.Append("\n").ToString();
        }

        /// <summary>
        /// Does the given menuitem exist?
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool CheckInput(char input)
        {
            return Menus[CurrentMenu].ContainsKey(input);
        }

        /// <summary>
        /// Run the given menuitem.
        /// </summary>
        /// <param name="input"></param>
        public static void SelectItem(char input)
        {
            Menus[CurrentMenu][input].Run();
        }
    }
}