﻿using System;
using DAL;
using Domain;
using static Utilities.Utilities;

namespace MenuSystem
{
    internal static class MenuActions
    {
        internal static void NewGameMenu()
        {
            Menu.CurrentMenu = "newGame";
        }

        internal static void NewGameRandom()
        {
            Launcher.LaunchRandom();
        }

        internal static void NewGameCustom()
        {
            Launcher.LaunchCustom();
        }

        internal static void LoadGameMenu()
        {
            LoadGameMenu(null);
        }

        private static void LoadGameMenu(PagedTableView tbl)
        {
            GetSaveAndReplayMenu(tbl, false);
        }

        internal static void ReplayMenu()
        {
            ReplayMenu(null);
        }

        private static void ReplayMenu(PagedTableView tbl)
        {
            GetSaveAndReplayMenu(tbl, true);
        }

        /// <summary>
        /// Sets up the menu screen seen in both load game and replay.
        /// </summary>
        /// <param name="tbl"></param>
        /// <param name="gameOver"></param>
        private static void GetSaveAndReplayMenu(PagedTableView tbl, bool gameOver)
        {
            string desc = "";
            if (gameOver) desc = "replay";
            else desc = "save";
            Console.Clear();
            Console.WriteLine("Choose a " + desc + " to load. Press n for next page, b to go back to previous page, and m to return to main menu!");
            Console.WriteLine("To remove a " + desc + ", write the " + desc + "'s number and the letter r (e.g. 1r)!");
            if (tbl == null) tbl = new PagedTableView(gameOver);
            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.Write(tbl.Render());
            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.WriteLine("Page " + tbl.CurrentPage + " out of " + tbl.PageCount());
            string input = Console.ReadLine().Trim().ToLower();
            // remove part
            if (input.Length == 2 && int.TryParse(input.Substring(0, 1), out int remove) && input.Substring(1, 1).Equals("r"))
            {
                Console.WriteLine("Remove " + desc + " nr " + remove + "? (y/n)");
                string answer = Console.ReadLine().Trim().ToLower();
                if ("y".Equals(answer)) tbl.Remove(remove);

                LoadGameMenu(tbl);
            }
            // construct new game or replay
            else if (int.TryParse(input, out int save))
            {
                if (!gameOver) Launcher.LaunchSaved(tbl.Get(save));
                else Launcher.LaunchReplay(tbl.Get(save));
            }
            //next page
            else if ("n".Equals(input))
            {
                tbl.NextPage();
                LoadGameMenu(tbl);
            }
            // prev page
            else if ("b".Equals(input))
            {
                tbl.PrevPage();
                LoadGameMenu(tbl);
            }
            // returns to main menu
            else if ("m".Equals(input)) BackToMain();
        }

        internal static void Options()
        {
            Menu.CurrentMenu = "options";
        }

        internal static void ShipsMayTouch()
        {
            Console.Clear();
            Console.WriteLine("Currently shipsMayTouch = " + Rules.shipsMayTouch);
            Console.WriteLine("Press enter to go back or t to toggle it!");

            string input = Console.ReadLine().Trim().ToLower();
            if (input.Equals("t"))
            {
                Rules.shipsMayTouch = !Rules.shipsMayTouch;
                var rulesDal = AppDbContext.appDbContext.GetPrimaryRulesDAL();
                rulesDal.ShipsMayTouch = Rules.shipsMayTouch;
                AppDbContext.appDbContext.SaveChanges();
                ShipsMayTouch();
            }
        }

        internal static void EditMapSize()
        {
            Console.Clear();
            Console.WriteLine("Currently the the map is " + Rules.width + " by " + Rules.height + " squares.");
            Console.WriteLine("Press enter to go back or type a size to change it (e.g. 10x10)!");
            Console.WriteLine("min 5x5, max " + Alphabet.Count + "x" + 99);

            string[] input = Console.ReadLine().Trim().ToLower().Split('x');
            if (input.Length == 2 && int.TryParse(input[0], out int col) && int.TryParse(input[1], out int row))
            {
                if (col < 5 || col > Alphabet.Count || row < 5 || row > 99)
                {
                    Console.WriteLine("Invalid map size!");
                }
                else
                {
                    Rules.width = col;
                    Rules.height = row;
                    var rulesDal = AppDbContext.appDbContext.GetPrimaryRulesDAL();
                    rulesDal.Width = Rules.width;
                    rulesDal.Height = Rules.height;
                    AppDbContext.appDbContext.SaveChanges();
                }

                EditMapSize();
            }
        }

        internal static void EditFleetSize()
        {
            Console.Clear();
            Console.WriteLine("Currently fleet consists of:");
            foreach (var entry in Rules.FleetSize)
            {
                Console.WriteLine(entry.Value + " " + entry.Key + "(s)");
            }

            Console.WriteLine(
                "Press enter to go back or insert the two first letters of the desired ship and edit its fleetCount.");
            Console.WriteLine("e.g. ca2 (means two carriers)");

            string input = Console.ReadLine().Trim().ToLower();
            if ((input.Length == 3 || input.Length == 4) && int.TryParse(input.Substring(2), out int shipCount))
            {
                switch (input.Substring(0, 2))
                {
                    case "ca":
                        Rules.FleetSize["carrier"] = shipCount;
                        var rulesDal = AppDbContext.appDbContext.GetPrimaryRulesDAL();
                        rulesDal.CarrierCount = shipCount;
                        AppDbContext.appDbContext.SaveChanges();
                        break;

                    case "ba":
                        Rules.FleetSize["battleship"] = shipCount;
                        rulesDal = AppDbContext.appDbContext.GetPrimaryRulesDAL();
                        rulesDal.BattleshipCount = shipCount;
                        AppDbContext.appDbContext.SaveChanges();
                        break;

                    case "su":
                        Rules.FleetSize["submarine"] = shipCount;
                        rulesDal = AppDbContext.appDbContext.GetPrimaryRulesDAL();
                        rulesDal.SubmarineCount = shipCount;
                        AppDbContext.appDbContext.SaveChanges();
                        break;

                    case "cr":
                        Rules.FleetSize["cruiser"] = shipCount;
                        rulesDal = AppDbContext.appDbContext.GetPrimaryRulesDAL();
                        rulesDal.CruiserCount = shipCount;
                        AppDbContext.appDbContext.SaveChanges();
                        break;

                    case "pa":
                        Rules.FleetSize["patrol"] = shipCount;
                        rulesDal = AppDbContext.appDbContext.GetPrimaryRulesDAL();
                        rulesDal.PatrolCount = shipCount;
                        AppDbContext.appDbContext.SaveChanges();
                        break;

                    default:
                        Console.WriteLine("No ship selected!");
                        break;
                }

                EditFleetSize();
            }
        }

        internal static void EditShipSizes()
        {
            Console.Clear();
            Console.WriteLine("Currently ship sizes are:");
            foreach (var entry in Rules.ShipSizes)
            {
                Console.WriteLine(entry.Value + " squares for " + entry.Key);
            }

            Console.WriteLine(
                "Press enter to go back or insert the two first letters of the desired ship and edit its fleetCount.");
            Console.WriteLine("e.g. ca6 (carrier's length is now six)");

            string input = Console.ReadLine().Trim().ToLower();
            if ((input.Length == 3 || input.Length == 4) && int.TryParse(input.Substring(2), out int shipLength))
            {
                switch (input.Substring(0, 2))
                {
                    case "ca":
                        Rules.ShipSizes["carrier"] = shipLength;
                        var rulesDal = AppDbContext.appDbContext.GetPrimaryRulesDAL();
                        rulesDal.CarrierSize = shipLength;
                        AppDbContext.appDbContext.SaveChanges();
                        break;

                    case "ba":
                        Rules.ShipSizes["battleship"] = shipLength;
                        rulesDal = AppDbContext.appDbContext.GetPrimaryRulesDAL();
                        rulesDal.BattleshipSize = shipLength;
                        AppDbContext.appDbContext.SaveChanges();
                        break;

                    case "su":
                        Rules.ShipSizes["submarine"] = shipLength;
                        rulesDal = AppDbContext.appDbContext.GetPrimaryRulesDAL();
                        rulesDal.SubmarineSize = shipLength;
                        AppDbContext.appDbContext.SaveChanges();
                        break;

                    case "cr":
                        Rules.ShipSizes["cruiser"] = shipLength;
                        rulesDal = AppDbContext.appDbContext.GetPrimaryRulesDAL();
                        rulesDal.CruiserSize = shipLength;
                        AppDbContext.appDbContext.SaveChanges();
                        break;

                    case "pa":
                        Rules.ShipSizes["patrol"] = shipLength;
                        rulesDal = AppDbContext.appDbContext.GetPrimaryRulesDAL();
                        rulesDal.PatrolSize = shipLength;
                        AppDbContext.appDbContext.SaveChanges();
                        break;

                    default:
                        Console.WriteLine("No ship selected!");
                        break;
                }

                EditShipSizes();
            }
        }

        internal static void BackToMain()
        {
            Menu.CurrentMenu = "main";
        }

        internal static void QuitGame()
        {
            Menu.Running = false;
        }
    }
}