﻿using System;
using System.Collections.Generic;

namespace MenuSystem
{
    /// <summary>
    /// A Represents one item in menu.
    /// </summary>
    internal class MenuItem
    {
        private char key;
        private string description;
        private MenuFunction action;

        /// <summary>
        /// Constructs a menuitem.
        /// </summary>
        /// <param name="key">Button to press.</param>
        /// <param name="description">What the user sees on the line.</param>
        /// <param name="action">The action upon pressing.</param>
        public MenuItem(char key, string description, MenuFunction action)
        {
            this.key = key;
            this.description = description;
            this.action = action;
        }

        /// <summary>
        /// Do the action, that the menuitem represents, when the user presses the menuitem.
        /// </summary>
        public void Run()
        {
            action();
        }

        /// <summary>
        /// Wrapper for the function pointer given for the menuitem constructor.
        /// </summary>
        public delegate void MenuFunction();

        /// <summary>
        /// Modifies toString behaviour.
        /// </summary>
        /// <returns>How one line looks in the menu.</returns>
        public override string ToString()
        {
            return key + " - " + description;
        }
    }
}