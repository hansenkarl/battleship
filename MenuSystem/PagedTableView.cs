﻿using DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace MenuSystem
{
    /// <summary>
    /// Creates load game and replay views. The only class that knows what the number in MenuActions.GetSaveAndReplayMenu() means. 
    /// </summary>
    public class PagedTableView
    {
        private readonly List<GameDAL> SavedGames = new List<GameDAL>();
        public int CurrentPage { get; set; } = 1;

        public PagedTableView(bool gameOver)
        {
            foreach (var saveGame in AppDbContext.appDbContext.GameDALs)
            {
                if (gameOver == saveGame.GameOver)
                {
                    // important for both load and save to work, DO NOT TOUCH!
                    saveGame.RulesDAL = AppDbContext.appDbContext.RulesDALs.Find(saveGame.RulesDALId);
                    SavedGames.Add(saveGame);
                }
            }
            SavedGames.Sort((x, y) => y.Timestamp.CompareTo(x.Timestamp));
        }


        public int PageCount()
        {
            return (int)Math.Ceiling(SavedGames.Count / 9.0);
        }
        

        public void NextPage()
        {
            
            if (CurrentPage < PageCount()) CurrentPage++;
        }

        public void PrevPage()
        {
            if (CurrentPage > 1) CurrentPage--;
        }

        public string Render()
        {
            int offset = 9 * (CurrentPage - 1);
            int chosenCount = 9;
            if (SavedGames.Count - offset < 9) chosenCount = SavedGames.Count - offset;
            var range = SavedGames.GetRange(offset, chosenCount);
            StringBuilder sb = new StringBuilder();
            for (int i = 1; i <= range.Count; i++)
            {
                var save = range[i - 1];
                sb.Append(i);
                sb.Append(" - ");
                sb.Append(Utilities.Utilities.FormatTime(save.Timestamp));
                sb.Append(" - ");
                sb.Append(save.SaveName);
                sb.Append("\n");
            }
            return sb.ToString();
        }
        /// <summary>
        /// Get a GameDAL (from database and console).
        /// </summary>
        /// <param name="nr"></param>
        /// <returns></returns>
        public GameDAL Get(int nr)
        {
            int offset = 9 * (CurrentPage - 1);
            int maxSaveNr = 9;
            if (SavedGames.Count - offset < 9) maxSaveNr = SavedGames.Count - offset;
            if (nr < 1 || nr > maxSaveNr) return null;
            return SavedGames[offset + nr - 1];
        }
        /// <summary>
        /// Gets a page. Pages consists of gameDALs.
        /// </summary>
        /// <param name="nr"></param>
        /// <returns></returns>
        public List<GameDAL> GetPage(int nr)
        {
            int offset = 9 * (CurrentPage - 1);
            int chosenCount = 9;
            if (SavedGames.Count - offset < 9) chosenCount = SavedGames.Count - offset;
            return SavedGames.GetRange(offset, chosenCount);
        }

        /// <summary>
        /// Delete a gameDAL (from database and console).
        /// </summary>
        /// <param name="nr"></param>
        public void Remove(int nr)
        {
            int offset = 9 * (CurrentPage - 1);
            int maxSaveNr = 9;
            if (SavedGames.Count - offset < 9) maxSaveNr = SavedGames.Count - offset;
            if (nr < 1 || nr > maxSaveNr) return;
            var toRemove = SavedGames[offset + nr - 1];
            SavedGames.Remove(toRemove);
            if (nr == maxSaveNr) PrevPage();
            AppDbContext.appDbContext.GameDALs.Remove(toRemove);
            AppDbContext.appDbContext.RulesDALs.Remove(toRemove.RulesDAL);
            AppDbContext.appDbContext.SaveChanges();
        }
    }
}
