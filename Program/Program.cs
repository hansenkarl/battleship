﻿using System;
using System.Collections.Generic;
using Domain;
using MenuSystem;

namespace Program
{
    class Program
    {
        static void Main(string[] args)
        {
            while (Menu.Running)
            {
                Rules.Init();
                Console.Clear();
                Console.Write(Menu.GetMenu());
                string input = Console.ReadLine().Trim().ToLower();
                if (input.Length != 1) continue;
                char c = input.ToCharArray()[0];
                if (Menu.CheckInput(c)) Menu.SelectItem(c);
            }
        }
    }
}