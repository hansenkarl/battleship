﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages
{
    public class GameModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public string UserInput { get; set; }

        public string GameScreen { get; set; }

        public static bool ShowCompleteScreen { get; set; }

        public static Game Game { get; set; }
        public IActionResult OnGet()
        {
            if (!ShowCompleteScreen) GameScreen = Game.RunOneTurn(UserInput);
            else GameScreen = Game.OutputWebLastShotScreen();

            if (Game.IsGameOver())
            {
                if (ShowCompleteScreen)
                {
                    if (Game.Player1HitCount == Game.ShipTileCount || Game.Player2HitCount == Game.ShipTileCount) return RedirectToPage("/GameOverScreen");
                    return RedirectToPage("/Index");
                }
                else ShowCompleteScreen = true;
            }
            return Page();
        }
    }
}