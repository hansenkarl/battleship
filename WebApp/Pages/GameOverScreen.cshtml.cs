﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages
{
    public class GameOverScreenModel : PageModel
    {
        public static Game Game { get; set; }

        public static bool Save { get; set; }

        [BindProperty(SupportsGet = true)]
        public string UserInput { get; set; }

        public void OnGet()
        {
            Game = GameModel.Game;
        }

        public IActionResult OnPost()
        {
            if (UserInput == null) { return RedirectToPage("/GameOverScreen"); }
            Save = true;           
            Game.SaveReplay(UserInput);
            return RedirectToPage("/GameOverScreen");
        }

        public string Winner()
        {
            int nr = 0;
            if (Game.Player1HitCount == Game.ShipTileCount) nr = 1;
            else nr = 2;
            return "Congratulations player " + nr + ", you won!";
        }
    }
}