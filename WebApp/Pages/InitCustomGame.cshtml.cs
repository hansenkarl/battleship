﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages
{
    public class InitCustomGameModel : PageModel
    {
        public string GameScreen { get; set; }

        [BindProperty(SupportsGet = true)]
        public string UserInput { get; set; }

        public string ShipName { get; set; }

        public static bool WarningScreen { get; set; }

        public static bool ShowCompleteMap { get; set; }

        public static int activePlayer;
        public static int passivePlayer;

        private static GameMap player1ShipMap;
        private static GameMap player2ShipMap;

        private static Dictionary<string, int> missingShipsP1;
        private static Dictionary<string, int> missingShipsP2;

        public IActionResult OnGet(string newGame)
        {
            WarningScreen = false;
            if (newGame != null)
            {
                WarningScreen = true;
                ShowCompleteMap = false;
                activePlayer = 1;
                passivePlayer = 2;
                missingShipsP1 = new Dictionary<string, int>(Rules.FleetSize);
                missingShipsP2 = new Dictionary<string, int>(Rules.FleetSize);
                player1ShipMap = new GameMap();
                player2ShipMap = new GameMap();
                return Page();
            }
            Dictionary<string, int> missingShips = (activePlayer == 1) ? missingShipsP1 : missingShipsP2;
            GameMap shipMap = (activePlayer == 1) ? player1ShipMap : player2ShipMap;
            ShipName = FindMissing(missingShips);
            if (ShipName == null && ShowCompleteMap)
            {
                ShowCompleteMap = false;
                if (activePlayer == 2) {
                    GameModel.Game = new Game(player1ShipMap, player2ShipMap);
                    GameModel.ShowCompleteScreen = false;
                    // to make starting the new game possible next time
                    newGame = null;
                    return RedirectToPage("/Game");
                }
                WarningScreen = true;
                activePlayer = 2;
                passivePlayer = 1;
                return Page();
            }
            if (ShipName == null) ShowCompleteMap = true;
            GameScreen = "";
            foreach (var line in shipMap.RenderMap())
            {
                GameScreen += line + "\n";
            }
            

            return Page();
        }

        public IActionResult OnPost()
        {
            if (!WarningScreen) 
            {
                if (UserInput == null)  return RedirectToPage("/InitCustomGame");
                string input = UserInput.Trim().ToLower();               
                Dictionary<string, int> missingShips = (activePlayer == 1) ? missingShipsP1 : missingShipsP2;
                GameMap shipMap = (activePlayer == 1) ? player1ShipMap : player2ShipMap;
                ShipName = FindMissing(missingShips);
                Ship ship = null;
                switch (ShipName)
                {
                    case "carrier":
                        ship = new Carrier();
                        break;
                    case "battleship":
                        ship = new Battleship();
                        break;
                    case "submarine":
                        ship = new Submarine();
                        break;
                    case "cruiser":
                        ship = new Cruiser();
                        break;
                    case "patrol":
                        ship = new Patrol();
                        break;
                }
                if (input.Equals("s")) missingShips[ShipName]--;
                if (input.EndsWith("+"))
                {
                    ship.Direction = Ship.Directions.West;
                    input = input.Substring(0, input.Length - 1);
                }
                else ship.Direction = Ship.Directions.North;

                if (!(input.Length != 2 && input.Length != 3))
                {
                    char col = input[0];
                    int.TryParse(input.Substring(1), out int row);

                    if (shipMap.SetShip(col, row, ship)) missingShips[ShipName]--;
                }                
            }
            return RedirectToPage("/InitCustomGame");
        }

        private string FindMissing(Dictionary<string, int> missingShips)
        {
            foreach(var entry in missingShips)
            {
                if (entry.Value > 0) return entry.Key;
            }
            return null;
        }
    }
}