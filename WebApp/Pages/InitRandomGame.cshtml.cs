﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages
{
    public class InitRandomGameModel : PageModel
    {
        public IActionResult OnGet()
        {
            GameMap player1ShipMap = new GameMap();
            GameMap player2ShipMap = new GameMap();
            Launcher.GenerateRandomStart(player1ShipMap);
            Launcher.GenerateRandomStart(player2ShipMap);
            GameModel.Game = new Game(player1ShipMap, player2ShipMap);
            GameModel.ShowCompleteScreen = false;

            return RedirectToPage("/Game");
        }
    }
}