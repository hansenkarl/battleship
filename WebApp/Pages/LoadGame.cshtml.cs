﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using MenuSystem;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages
{
    public class LoadGameModel : PageModel
    {
        public PagedTableView Tbv { get; set; }

        public List<GameDAL> TbvPage { get; set; }

        [BindProperty(SupportsGet = true)]
        public int CurPageNr { get; set; }


        public IActionResult OnGet(string action, int id, int page)
        {
            CurPageNr = page;
            if (CurPageNr == 0) CurPageNr = 1;

            Tbv = new PagedTableView(false)
            {
                CurrentPage = CurPageNr
            };

            if (action == "prev")
            {
                Tbv.PrevPage();
                CurPageNr = Tbv.CurrentPage;
            }
            if (action == "next")
            {
                Tbv.NextPage();
                CurPageNr = Tbv.CurrentPage;
            }

            TbvPage = Tbv.GetPage(CurPageNr);

            if (action == "load")
            {
                GameDAL gameDAL = Tbv.Get(id);
                var rules = gameDAL.RulesDAL;
                Rules.Load(rules);
                GameMap player1ShipMap = new GameMap(gameDAL.Player1Ships);
                GameMap player2ShipMap = new GameMap(gameDAL.Player2Ships);
                Game game = new Game(player1ShipMap, player2ShipMap)
                {
                    turns = Launcher.DeserializeTurns(gameDAL.Turns)
                };
                GameModel.ShowCompleteScreen = false;
                game.Simulate();
                GameModel.Game = game;
                return RedirectToPage("/Game");
            }

            if (action == "delete")
            {
                Tbv.Remove(id);
                CurPageNr = Tbv.CurrentPage;
                TbvPage = Tbv.GetPage(CurPageNr);
            }

            return Page();
        }
    }
}