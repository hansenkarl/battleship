﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages
{
    public class OptionsModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public bool ShipsMayTouch { get; set; }

        [BindProperty(SupportsGet = true)]
        public int Width { get; set; }

        [BindProperty(SupportsGet = true)]
        public int Height { get; set; }



        public void OnGet()
        {
            ShipsMayTouch = Rules.shipsMayTouch;
            Width = Rules.width;
            Height = Rules.height;
        }

        public IActionResult OnPost()
        {
            Rules.shipsMayTouch = ShipsMayTouch;
            Rules.width = Width;
            Rules.height = Height;
            foreach (var ship in Rules.ShipNames)
            {
                Rules.ShipSizes[ship] = int.Parse(Request.Form["s_" + ship]);
                Rules.FleetSize[ship] = int.Parse(Request.Form["c_" + ship]);
            }
                Rules.Save();
            return RedirectToPage("/Options");
        }
    }
}