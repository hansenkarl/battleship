﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages
{
    public class ReplayerModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public string UserInput { get; set; }

        public string GameScreen { get; set; }

        public static Replayer Replayer { get; set; }
        public IActionResult OnGet(bool next)
        {
            if (!next) GameScreen = Replayer.OutputMap();
            else
            {
                if (Replayer.turnCount <= Replayer.turns.Count) GameScreen = Replayer.RunOneTurn();
                else GameScreen = Replayer.OutputMap();
            }
            return Page();
        }
    }
}